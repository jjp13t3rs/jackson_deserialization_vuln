package nl.jjpieters;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class AccountApi {

    private static final ObjectMapper deserializer = new ObjectMapper().enableDefaultTyping();

    @PostMapping("/accounts")
    public Account test(@RequestBody String request) throws IOException {
        return deserializer.readValue(request, Account.class);
    }

    @GetMapping
    public String test() {
        return "Hello";
    }

}
